package dev.creepah.duels.stats;

public enum Statistic {
    WINS("Wins", 0),
    LOSSES("Losses", 0),
    RATING("Rating", 1500);

    private String string;
    private int defaultValue;

    Statistic(String string, int defaultValue) {
        this.string = string;
        this.defaultValue = defaultValue;
    }

    public String getString() {
        return string;
    }

    public int getDefaultValue() {
        return defaultValue;
    }
}
