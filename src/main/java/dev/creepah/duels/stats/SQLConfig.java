package dev.creepah.duels.stats;

import lombok.Data;
import org.bukkit.configuration.file.FileConfiguration;

@Data
public class SQLConfig {

    private final String username, password, database, hostname;
    private final int port;

    public static SQLConfig fromConfig(FileConfiguration config) {
        return new SQLConfig(config.getString("sql.username"),
                config.getString("sql.password"),
                config.getString("sql.database"),
                config.getString("sql.hostname"),
                config.getInt("sql.port"));
    }
}
