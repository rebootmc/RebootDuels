package dev.creepah.duels;

import com.google.gson.Gson;
import dev.creepah.duels.cmd.DuelsCommand;
import dev.creepah.duels.manager.*;
import dev.creepah.duels.stats.SQLConfig;
import lombok.Getter;
import tech.rayline.core.GsonBridge;
import tech.rayline.core.plugin.RedemptivePlugin;
import tech.rayline.core.plugin.UsesFormats;

import java.io.File;
import java.io.FileReader;

@UsesFormats
public final class RebootDuels extends RedemptivePlugin {

    private static RebootDuels instance;

    @Getter private ArenaManager arenaManager;
    @Getter private DuelsManager duelsManager;
    @Getter private KitManager kitManager;
    @Getter private QueueManager queueManager;
    @Getter private StatisticsManager statsManager;
    @Getter private RequestManager requestManager;

    private Gson gson = new GsonBridge(this).getGson();

    public static RebootDuels get() {
        return instance;
    }

    @Override
    protected void onModuleEnable() throws Exception {
        instance = this;

        kitManager = new KitManager();
        queueManager = new QueueManager();
        statsManager = new StatisticsManager(SQLConfig.fromConfig(getConfig()));
        arenaManager = new ArenaManager();
        duelsManager = new DuelsManager();
        requestManager = new RequestManager();

        arenaManager.loadArenas();
        kitManager.loadKits();

        registerCommand(new DuelsCommand());
        registerListener(new GameEventManager());
    }

    @Override
    protected void onModuleDisable() throws Exception {
        arenaManager.saveArenas();
        kitManager.saveKits();
    }

    public <T> T load(Class<T> type, File file) {
        try (FileReader reader = new FileReader(file)) {
            return gson.fromJson(reader, type);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
