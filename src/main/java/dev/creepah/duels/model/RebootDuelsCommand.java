package dev.creepah.duels.model;

import dev.creepah.duels.RebootDuels;
import org.bukkit.Sound;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import tech.rayline.core.command.CommandException;
import tech.rayline.core.command.PermissionException;
import tech.rayline.core.command.RDCommand;
import tech.rayline.core.command.UnhandledCommandExceptionException;
import tech.rayline.core.util.SoundUtil;

public abstract class RebootDuelsCommand extends RDCommand {

    public RebootDuelsCommand(String name) {
        super(name);
    }

    public RebootDuelsCommand(String name, RebootDuelsCommand... subCommands) {
        super(name, subCommands);
    }

    @Override
    protected void handleCommandException(CommandException ex, String[] args, CommandSender sender) {
        if (sender instanceof Player) SoundUtil.playTo((Player) sender, Sound.NOTE_BASS);

        if (ex instanceof UnhandledCommandExceptionException) {
            ((UnhandledCommandExceptionException) ex).getCausingException().printStackTrace();
            sender.sendMessage(RebootDuels.get().formatAt("error.unhandled").get());
        } else {
            sender.sendMessage(RebootDuels.get().formatAt("error.general").withModifier("message", ex.getMessage()).get());
        }
    }
}
