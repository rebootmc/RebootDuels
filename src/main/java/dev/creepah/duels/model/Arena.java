package dev.creepah.duels.model;

import lombok.Data;
import org.bukkit.Location;

@Data
public class Arena {

    private final String name;
    private Location locationOne, locationTwo;
}
