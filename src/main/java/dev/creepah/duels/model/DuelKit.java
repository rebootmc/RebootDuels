package dev.creepah.duels.model;

import lombok.Data;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;

import java.util.ArrayList;
import java.util.List;

@Data
public class DuelKit {

    private final String name;
    private List<KitItem> contents;
    private ItemStack[] armour;
    private Material icon;
    private boolean custom = false;

    public void loadFrom(Player player, boolean saveContents) {
        if (saveContents) {
            PlayerInventory inventory = player.getInventory();

            contents = new ArrayList<>();
            ItemStack[] inv = inventory.getContents();
            for (int i = 0; i < inv.length; i++) {
                if (inv[i] != null) contents.add(new KitItem(inv[i], i));
            }

            armour = inventory.getArmorContents();
        }

        icon = player.getItemInHand() == null || player.getItemInHand().getType() == Material.AIR
                ? Material.DIRT : player.getItemInHand().getType();
    }

    public String tableName() {
        return name.toLowerCase().replace(" ", "_");
    }

    @Data
    public static class KitItem {
        private final ItemStack stack;
        private final int slot;
    }
}
