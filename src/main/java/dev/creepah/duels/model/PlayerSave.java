package dev.creepah.duels.model;

import lombok.Data;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;

import java.util.Collection;

@Data
public class PlayerSave {

    private ItemStack[] inventory;
    private ItemStack[] armour;
    private double health;
    private int foodLevel;
    private Location location;
    private Collection<PotionEffect> potionEffects;
    private float experience;
    private GameMode mode;

    public PlayerSave(Player player, boolean saveContents) {
        if (saveContents) {
            inventory = player.getInventory().getContents();
            armour = player.getInventory().getArmorContents();
        }

        health = player.getHealth();
        foodLevel = player.getFoodLevel();
        location = player.getLocation().clone().add(0, 1, 0);
        potionEffects = player.getActivePotionEffects();
        experience = player.getExp();
        mode = player.getGameMode();
    }
}
