package dev.creepah.duels.model;

import dev.creepah.duels.RebootDuels;

public enum DuelState {

    PRETELEPORT(5, RebootDuels.get().formatAt("duel.pre-teleport").get()),
    PREDUEL(3, RebootDuels.get().formatAt("duel.pre-duel").get()),
    PVP(300, null),
    POSTDUEL(5, null);

    private int duration; // The duration of this duel state.
    private String message; // The message shown each second during this state (null if no message).

    DuelState(int duration, String message) {
        this.duration = duration;
        this.message = message;
    }

    public int getDuration() {
        return duration;
    }

    public String getMessage() {
        return message;
    }
}
