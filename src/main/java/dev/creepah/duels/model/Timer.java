package dev.creepah.duels.model;

import dev.creepah.duels.RebootDuels;
import dev.creepah.duels.manager.DuelsManager;
import lombok.Getter;
import lombok.Setter;
import org.bukkit.Sound;
import org.bukkit.scheduler.BukkitRunnable;
import tech.rayline.core.util.SoundUtil;

@Getter
@Setter
public class Timer extends BukkitRunnable {

    private Duel duel;
    private int time;

    public Timer(Duel duel) {
        this.duel = duel;
    }

    DuelsManager manager = RebootDuels.get().getDuelsManager();

    @Override
    public void run() {
        DuelState state = duel.getState();

        // Send the state's message if there is one.
        if (state.getMessage() != null && time != 0) duel.send(state.getMessage().replace("{{time}}", String.valueOf(time)));

        // Play sounds for countdown.
        if ((state == DuelState.PREDUEL || state == DuelState.PRETELEPORT) && time != 0) {
            for (DuelPlayer player : duel.getBoth()) SoundUtil.playTo(player.getPlayer(), Sound.NOTE_BASS);
        }

        // Handle the timer reaching it's end.
        if (time == 0) {
            switch (state) {
                case PRETELEPORT:
                    duel.setState(DuelState.PREDUEL);
                    manager.teleportPlayers(duel);
                    time = state.getDuration();
                    break;
                case PREDUEL:
                    duel.setState(DuelState.PVP);
                    manager.allowPVP(duel);
                    time = DuelState.PVP.getDuration();
                    break;
                case PVP:
                    duel.setState(DuelState.POSTDUEL);
                    manager.cancelDuel(duel, "Duel took too long", true);
                    break;
                case POSTDUEL:
                    manager.finish(duel);
                    cancel();
                    break;
            }
            return;
        }

        time--;
    }
}
