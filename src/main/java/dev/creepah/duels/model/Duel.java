package dev.creepah.duels.model;

import dev.creepah.duels.RebootDuels;
import lombok.Data;

@Data
public class Duel {

    private DuelPlayer playerOne, playerTwo;
    private DuelKit kit;
    private DuelState state;
    private Timer timer;
    private boolean friendly;

    public Duel(DuelPlayer one, DuelPlayer two, DuelKit kit) {
        playerOne = one;
        playerTwo = two;
        this.kit = kit;

        state = DuelState.PRETELEPORT;

        timer = new Timer(this);
        timer.runTaskTimer(RebootDuels.get(), 0, 20);
        timer.setTime(state.getDuration());

        friendly = false; // False unless otherwise defined.
    }

    // Send a message to both players involved in the duel.
    public void send(String message) {
        if (playerOne.getPlayer() != null) playerOne.getPlayer().sendMessage(message);
        if (playerTwo.getPlayer() != null) playerTwo.getPlayer().sendMessage(message);
    }

    // Returns a duel player in this duel from the specified name.
    public DuelPlayer getByName(String name) {
        if (playerOne.getPlayer().getName().equals(name)) return playerOne;
        else if (playerTwo.getPlayer().getName().equals(name)) return playerTwo;
        else return null;
    }

    // Returns the relevant duel player from provided number.
    public DuelPlayer get(int number) {
        return number == 1 ? playerOne : playerTwo;
    }

    // Returns both players in a player array.
    public DuelPlayer[] getBoth() {
        return new DuelPlayer[] { playerOne, playerTwo };
    }
}
