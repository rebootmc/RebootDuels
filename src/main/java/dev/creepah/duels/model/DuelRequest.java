package dev.creepah.duels.model;

import dev.creepah.duels.RebootDuels;
import dev.creepah.duels.manager.RequestManager;
import lombok.Data;
import org.bukkit.entity.Player;

@Data
public class DuelRequest {

    private final Player requester;
    private final Player requestee;
    private final DuelKit kit;

    transient RequestManager requestManager = RebootDuels.get().getRequestManager();

    // Initiate a duel based upon this request.
    public void accept() {
        requestManager.getRequests().remove(this);

        Arena arena = RebootDuels.get().getArenaManager().getFreeArena();
        if (arena != null) {
            RebootDuels.get().getDuelsManager().start(arena, requester, requestee, kit, true);
        } else {
            requester.sendMessage(RebootDuels.get().formatAt("arena.none-available").get());
            requestee.sendMessage(RebootDuels.get().formatAt("arena.none-available").get());
        }
    }

    // Deny this request.
    public void deny() {
        requestManager.getRequests().remove(this);

        requester.sendMessage(RebootDuels.get().formatAt("request.denied").withModifier("requestee", requestee.getName()).get());
        requestee.sendMessage(RebootDuels.get().formatAt("request.deny").get());
    }
}
