package dev.creepah.duels.model;

import dev.creepah.duels.RebootDuels;
import dev.creepah.duels.manager.StatisticsManager;
import dev.creepah.duels.stats.Statistic;
import lombok.Data;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import tech.rayline.core.util.PlayerUtil;

import java.sql.SQLException;

@Data
public class DuelPlayer {

    private final Player player;
    private PlayerSave save;

    StatisticsManager statsManager = RebootDuels.get().getStatsManager();

    // Restores a player's previous state.
    public void restore(boolean restoreContents) {
        PlayerUtil.resetPlayer(player);

        if (restoreContents) {
            player.getInventory().setContents(save.getInventory());
            player.getInventory().setArmorContents(save.getArmour());
        }

        player.setGameMode(save.getMode());
        player.setHealth(save.getHealth());
        player.setFoodLevel(save.getFoodLevel());
        save.getPotionEffects().forEach(player::addPotionEffect);
        player.setExp(save.getExperience());
        player.teleport(save.getLocation());
    }

    // Get the duel this duel player is involved in.
    public Duel getDuel() {
        for (Duel duel : RebootDuels.get().getDuelsManager().getAllDuels()) {
            if (duel.get(1).equals(this)) return duel;
            if (duel.get(2).equals(this)) return duel;
        }
        return null;
    }

    // Get the arena location relevant to this player.
    public Location getLocation() {
        Arena arena = RebootDuels.get().getArenaManager().getAssociatedArena(getDuel());
        if (getDuel().get(1).getPlayer().getUniqueId().equals(player.getUniqueId())) return arena.getLocationOne();
        if (getDuel().get(2).getPlayer().getUniqueId().equals(player.getUniqueId())) return arena.getLocationTwo();

        return null;
    }

    // Get the name of this duel player.
    public String name() {
        return getPlayer().getName();
    }

    // Returns the statistic provided for this duel player.
    public int getStat(Statistic statistic) {
        try {
            return statsManager.getStat(player.getUniqueId(), getDuel().getKit(), statistic);
        } catch (SQLException e) {
            e.printStackTrace();
            return -1;
        }
    }

    // Set the specified statistic to the provided value.
    public void setStat(Statistic statistic, int value) {
        try {
            statsManager.setStat(player.getUniqueId(), getDuel().getKit(), statistic, value);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    // Modify one of this duel player's statistics.
    public void modifyStat(Statistic statistic, int num) {
        setStat(statistic, getStat(statistic) + num);
    }
}
