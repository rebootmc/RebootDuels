package dev.creepah.duels.manager;

import dev.creepah.duels.RebootDuels;
import dev.creepah.duels.model.*;
import dev.creepah.duels.stats.Statistic;
import lombok.Getter;
import org.bukkit.GameMode;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import tech.rayline.core.util.GeneralUtils;
import tech.rayline.core.util.Point;
import tech.rayline.core.util.SoundUtil;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

public class DuelsManager {

    @Getter private Map<Arena, Duel> activeDuels = new HashMap<>();

    ArenaManager arenaManager = RebootDuels.get().getArenaManager();
    KitManager kitManager = RebootDuels.get().getKitManager();
    StatisticsManager statsManager = RebootDuels.get().getStatsManager();
    QueueManager queueManager = RebootDuels.get().getQueueManager();

    // Start a duel between the two specified players.
    public void start(Arena arena, Player one, Player two, DuelKit kit, boolean friendly) {
        Duel duel = new Duel(new DuelPlayer(one), new DuelPlayer(two), kit);
        activeDuels.put(arena, duel);
        duel.setFriendly(friendly);

        for (DuelPlayer player : duel.getBoth()) {
            player.getPlayer().sendMessage(RebootDuels.get().formatAt("duel.start").withModifier("other", getOther(player).name()).get());

            if (queueManager.containsPlayer(player.getPlayer(), kit)) {
                queueManager.removeFrom(player.getPlayer());
            }
        }
    }

    // Save, teleport and give the kit to the players of the specified duel.
    public void teleportPlayers(Duel duel) {
        for (DuelPlayer player : duel.getBoth()) {
            player.setSave(new PlayerSave(player.getPlayer(), !duel.getKit().isCustom()));
            kitManager.give(player.getPlayer(), duel.getKit());
            player.getPlayer().setWalkSpeed(0f);
        }

        Arena arena = arenaManager.getAssociatedArena(duel);
        duel.get(1).getPlayer().teleport(arena.getLocationOne());
        duel.get(2).getPlayer().teleport(arena.getLocationTwo());
    }

    // Begin the PVP stage of the specified duel.
    public void allowPVP(Duel duel) {
        for (DuelPlayer player : duel.getBoth()) {
            player.getPlayer().setWalkSpeed(0.2f);
            SoundUtil.playTo(player.getPlayer(), Sound.SUCCESSFUL_HIT);
        }
    }

    // Handle a win for the specified duel.
    public void handleWin(Duel duel, Player winner) {
        duel.send(RebootDuels.get().formatAt("duel.winner").withModifier("winner", winner.getName()).get());

        duel.setState(DuelState.POSTDUEL);
        duel.getTimer().setTime(duel.getState().getDuration());

        GeneralUtils.randomlySpawnFireworks(RebootDuels.get(),
                winner.getWorld(),
                Point.of(winner.getLocation()),
                new Point(1D, 1D, 1D, 0F, 0F),
                12);

        if (!duel.isFriendly()) updateStatistics(getDuelPlayer(winner), getOther(getDuelPlayer(winner)));

        for (DuelPlayer player : duel.getBoth()) {
            Player p = player.getPlayer();

            if (p != null) {
                p.setGameMode(GameMode.SPECTATOR);
                if (!duel.getKit().isCustom()) p.getInventory().clear();
            }
        }
    }

    // Cleans up and finishes the specified duel.
    public void finish(Duel duel) {
        for (DuelPlayer player : duel.getBoth()) {
            if (player.getPlayer() != null) player.restore(!duel.getKit().isCustom());
        }

        activeDuels.remove(arenaManager.getAssociatedArena(duel));
    }

    // Cancels the specified duel for the specified reason.
    public void cancelDuel(Duel duel, String reason, boolean restore) {
        for (DuelPlayer player : duel.getBoth()) {
            if (player.getPlayer() != null && restore) player.restore(!duel.getKit().isCustom());
            SoundUtil.playTo(player.getPlayer(), Sound.ANVIL_LAND);
        }

        duel.send(RebootDuels.get().formatAt("duel.cancelled").withModifier("reason", reason).get());

        duel.getTimer().cancel();
        activeDuels.remove(arenaManager.getAssociatedArena(duel));
    }

    // Attempt to safely handle a player leaving during a duel.
    public void handleQuit(Duel duel, Player whoLeft) {
        switch (duel.getState()) {
            case PRETELEPORT:
                cancelDuel(duel, whoLeft.getName() + " logged out", false);
                break;
            case PREDUEL:
                cancelDuel(duel, whoLeft.getName() + " logged out!", true);
                break;
            case PVP:
                getDuelPlayer(whoLeft).restore(!duel.getKit().isCustom());
                handleWin(duel, getOther(getDuelPlayer(whoLeft)).getPlayer());
                break;
            case POSTDUEL:
                getDuelPlayer(whoLeft).restore(!duel.getKit().isCustom());
                break;
        }
    }

    // Update relevant statistics for the winner and loser of a duel.
    public void updateStatistics(DuelPlayer winner, DuelPlayer loser) {
        int winnerPrev = winner.getStat(Statistic.RATING);
        int loserPrev = loser.getStat(Statistic.RATING);

        // Calculate new ratings.
        int[] values = statsManager.getNewValues(winnerPrev, loserPrev);

        // Update ratings.
        winner.setStat(Statistic.RATING, values[0]);
        loser.setStat(Statistic.RATING, values[1]);

        // Notify players of their rating change;
        winner.getPlayer().sendMessage(RebootDuels.get().formatAt("duel.stat-diff")
                .withModifier("change", "&a+" + (values[0] - winnerPrev)).get());

        loser.getPlayer().sendMessage(RebootDuels.get().formatAt("duel.stat-diff")
                .withModifier("change", "&c" + (values[1] - loserPrev)).get());

        winner.modifyStat(Statistic.WINS, 1);
        loser.modifyStat(Statistic.LOSSES, 1);
    }

    // Returns the duel the specified player is currently in, or null if they aren't in a duel.
    public Duel getDuel(Player player) {
        for (Duel duel : getAllDuels()) if (duel.getByName(player.getName()) != null) return duel;
        return null;
    }

    // Returns a set of all duels.
    public Set<Duel> getAllDuels() {
        return activeDuels.keySet().stream().map(arena -> activeDuels.get(arena)).collect(Collectors.toSet());
    }

    // Get the other player in a duel with the specified player.
    public DuelPlayer getOther(DuelPlayer player) {
        Duel duel = player.getDuel();
        if (duel.get(1).equals(player)) return duel.get(2);
        else return duel.get(1);
    }

    // Returns a duel player instance from the specified player.
    public DuelPlayer getDuelPlayer(Player player) {
        for (Duel duel : getAllDuels()) {
            if (duel.get(1).getPlayer().equals(player)) return duel.get(1);
            if (duel.get(2).getPlayer().equals(player)) return duel.get(2);
        }
        return null;
    }
}
