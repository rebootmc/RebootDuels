package dev.creepah.duels.manager;

import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.ProtocolLibrary;
import com.comphenix.protocol.ProtocolManager;
import com.comphenix.protocol.events.PacketContainer;
import com.comphenix.protocol.wrappers.EnumWrappers;
import com.gmail.nossr50.events.experience.McMMOPlayerExperienceEvent;
import com.gmail.nossr50.events.skills.abilities.McMMOPlayerAbilityActivateEvent;
import com.gmail.nossr50.runnables.skills.BleedTimerTask;
import dev.creepah.duels.RebootDuels;
import dev.creepah.duels.model.Duel;
import dev.creepah.duels.model.DuelPlayer;
import dev.creepah.duels.model.DuelState;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.*;
import tech.rayline.core.util.RunnableShorthand;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class GameEventManager implements Listener {

    DuelsManager duelsManager = RebootDuels.get().getDuelsManager();
    Map<UUID, Location> deathLocations = new HashMap<>();

    // Prevent and check for movement in certain duel stages.
    @EventHandler
    public void onMove(PlayerMoveEvent event) {
        Player player = event.getPlayer();
        Duel duel = duelsManager.getDuel(player);

        if (duel != null) {
            if (event.getFrom().getX() != event.getTo().getX() && event.getFrom().getZ() != event.getTo().getZ()) {
                // If a player moves during pre-duel stages (in the arena).
                if (duel.getState() == DuelState.PREDUEL) {
                    Location location = duelsManager.getDuelPlayer(player).getLocation();
                    if (player.getLocation().distanceSquared(location) > 1) {
                        player.teleport(location);
                    }
                }
            }
        }
    }

    // Handle a death in the duel arena.
    @EventHandler
    public void onDeath(PlayerDeathEvent event) {
        DuelPlayer player = duelsManager.getDuelPlayer(event.getEntity());

        if (player != null) {
            Duel duel = player.getDuel();
            event.setDeathMessage("");

            Bukkit.getScheduler().runTaskLater(RebootDuels.get(), () -> {
                ProtocolManager protocolManager = ProtocolLibrary.getProtocolManager();
                PacketContainer packet = new PacketContainer(PacketType.Play.Client.CLIENT_COMMAND);
                packet.getClientCommands().write(0, EnumWrappers.ClientCommand.PERFORM_RESPAWN);

                try {
                    protocolManager.recieveClientPacket(player.getPlayer(), packet);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }, 1L);

            deathLocations.put(event.getEntity().getUniqueId(), player.getPlayer().getLocation());

            duelsManager.handleWin(duel, duelsManager.getOther(player).getPlayer());

            // Remove heads which might still be about after someone has been killed.
            Bukkit.getScheduler().scheduleSyncDelayedTask(RebootDuels.get(), () ->
                    player.getPlayer().getNearbyEntities(10, 10, 10)
                            .stream().filter(e -> e instanceof Item).forEach(e -> {
                Item i = (Item) e;
                Material type = i.getItemStack().getType();

                if (type == Material.SKULL || type == Material.SKULL_ITEM) {
                    e.remove();
                }
            }), 5);

            if (duel.getKit().isCustom()) return;
            event.getDrops().clear();
        }
    }

    // Ensure the player respawns back in the duel arena.
    @EventHandler
    public void onRespawn(PlayerRespawnEvent event) {
        DuelPlayer player = duelsManager.getDuelPlayer(event.getPlayer());

        if (player != null) {
            // To be very sure they get returned to the correct place!
            event.setRespawnLocation(deathLocations.get(player.getPlayer().getUniqueId()));
            event.getPlayer().teleport(deathLocations.get(player.getPlayer().getUniqueId()));
            deathLocations.remove(player.getPlayer().getUniqueId());
        }
    }

    // Prevent damage in non-pvp stages of the duel + ensure damage in pvp (factions bypass)
    @EventHandler(priority = EventPriority.LOWEST)
    public void onDamage(EntityDamageByEntityEvent event) {
        if (event.getEntity() instanceof Player && event.getDamager() instanceof Player) {
            DuelPlayer player = duelsManager.getDuelPlayer((Player) event.getEntity());
            DuelPlayer damager = duelsManager.getDuelPlayer((Player) event.getDamager());

            // Ensure that we're dealing with a duel related damage event.
            if (player != null) {
                Duel duel = player.getDuel();
                
                // Instantly stop players bleeding if they are bleeding in duels.
                if (BleedTimerTask.isBleeding(player.getPlayer())) {
                    BleedTimerTask.remove(player.getPlayer());
                }

                if (damager != null) {
                    if (duel.getState() != DuelState.PVP) {
                        event.setCancelled(true);
                    } else event.setCancelled(false);
                } else {
                    if (duel.getState() == DuelState.PRETELEPORT) {
                        duelsManager.cancelDuel(duel, player.name() + " entered combat!", false);
                    }
                }
            }
        }
    }

    // Cancel blocks being broken in duels.
    @EventHandler
    public void onBreak(BlockBreakEvent event) {
        DuelPlayer player = duelsManager.getDuelPlayer(event.getPlayer());
        if (player != null) {
            event.setCancelled(true);
        }
    }

    // Cancel blocks being placed in duels.
    @EventHandler
    public void onPlace(BlockPlaceEvent event) {
        DuelPlayer player = duelsManager.getDuelPlayer(event.getPlayer());
        if (player != null) {
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onMcMMOAbility(McMMOPlayerAbilityActivateEvent event) {
        DuelPlayer player = duelsManager.getDuelPlayer(event.getPlayer());
        if (player != null) {
            event.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onMcMMO(McMMOPlayerExperienceEvent event) {
        DuelPlayer player = duelsManager.getDuelPlayer(event.getPlayer());
        if (player != null) {
            event.setCancelled(true);
        }
    }

    // Prevent players issuing certain commands during a duel.
    @EventHandler
    public void onCommand(PlayerCommandPreprocessEvent event) {
        DuelPlayer player = duelsManager.getDuelPlayer(event.getPlayer());
        if (player != null) {
            RebootDuels.get().getConfig().getStringList("cmd-whitelist").stream()
                    .filter(cmd -> !event.getMessage().contains(cmd))
                    .forEach(cmd -> {
                event.setCancelled(true);
                player.getPlayer().sendMessage(RebootDuels.get().formatAt("duel.cmd-cancel").get());
            });
        }
    }

    // Handles players leaving during duels.
    @EventHandler
    public void onQuit(PlayerQuitEvent event) {
        if (duelsManager.getDuelPlayer(event.getPlayer()) != null) {
            duelsManager.handleQuit(duelsManager.getDuel(event.getPlayer()), event.getPlayer());
        }

        QueueManager queueManager = RebootDuels.get().getQueueManager();
        if (queueManager.getQueue(event.getPlayer()) != null) {
            queueManager.removeFrom(event.getPlayer());
        }
    }

    // Ensure that items aren't dropped in the duel arena.
    @EventHandler
    public void onDrop(PlayerDropItemEvent event) {
        if (duelsManager.getDuelPlayer(event.getPlayer()) != null) event.setCancelled(true);
    }

}
