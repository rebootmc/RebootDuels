package dev.creepah.duels.manager;

import dev.creepah.duels.RebootDuels;
import dev.creepah.duels.model.Arena;
import dev.creepah.duels.model.DuelKit;
import lombok.Getter;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.*;

public class QueueManager implements Runnable {

    @Getter private Map<DuelKit, List<UUID>> queue = new HashMap<>();

    public QueueManager() {
        int updateInterval = RebootDuels.get().getConfig().getInt("queue-update-interval");

        for (DuelKit kit : RebootDuels.get().getKitManager().getKits()) {
            queue.put(kit, new ArrayList<>());
        }

        Bukkit.getScheduler().runTaskTimer(RebootDuels.get(), this, 20 * 3, 20 * updateInterval);
    }

    // This code will be executed each interval, and will 'process' the duel queue.
    @Override
    public void run() {
        for (DuelKit kit : queue.keySet()) {
            List<UUID> players = queue.get(kit);

            if (players == null) {
                queue.put(kit, new ArrayList<>());
                return;
            }

            while (players.size() > 1) {
                Arena arena = RebootDuels.get().getArenaManager().getFreeArena();
                if (arena == null) return;

                Random random = new Random();
                UUID one = players.get(random.nextInt(players.size()));
                UUID two = players.get(random.nextInt(players.size()));

                // Continue picking random players in the queue until a match is found.
                while (one == two) {
                    one = players.get(random.nextInt(players.size()));
                    two = players.get(random.nextInt(players.size()));
                }

                RebootDuels.get().getDuelsManager().start(arena, Bukkit.getPlayer(one), Bukkit.getPlayer(two), kit, false);

                players.remove(one);
                players.remove(two);
            }
        }
    }

    // Adds the specified player to the queue for the specified kit.
    public void addTo(DuelKit kit, Player player) {
        if (queue.get(kit) == null) {
            List<UUID> list = new ArrayList<>();
            list.add(player.getUniqueId());
            queue.put(kit, list);
        } else queue.get(kit).add(player.getUniqueId());
        player.sendMessage(RebootDuels.get().formatAt("queue.added").withModifier("kit", kit.getName()).get());
    }

    // Removes the specified player from the duel queue.
    public void removeFrom(Player player) {
        List<UUID> players = getQueue(player);
        UUID toRemove = null;

        for (UUID u : players) {
            if (u.equals(player.getUniqueId())) toRemove = u;
        }

        players.remove(toRemove);
    }

    // Returns the queue with the specified player in it.
    public List<UUID> getQueue(Player player) {
        for (DuelKit kit : queue.keySet()) {
            if (containsPlayer(player, kit)) return queue.get(kit);
        }
        return null;
    }

    // Returns true if the provided kit's queue contains the provided player.
    public boolean containsPlayer(Player player, DuelKit kit) {
        if (queue.get(kit) == null) return false;
        for (UUID p : queue.get(kit)) {
            if (p.equals(player.getUniqueId())) return true;
        }
        return false;
    }

}
