package dev.creepah.duels.manager;

import dev.creepah.duels.RebootDuels;
import dev.creepah.duels.model.DuelKit;
import dev.creepah.duels.model.DuelRequest;
import lombok.Getter;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

public class RequestManager {

    @Getter private List<DuelRequest> requests = new ArrayList<>();

    // Request the provided requestee to duel.
    public void request(Player requester, Player requestee, DuelKit kit) {
        DuelRequest request = new DuelRequest(requester, requestee, kit);

        requestee.sendMessage(RebootDuels.get().formatAt("request.received")
                .withModifier("requester", requester.getName())
                .withModifier("kit", request.getKit().getName())
                .get());

        requester.sendMessage(RebootDuels.get().formatAt("request.sent").withModifier("requestee", requestee.getName()).get());

        requests.add(request);
    }

    // Returns a duel request involving the two specified players.
    public DuelRequest getRequestInvolving(Player one, Player two) {
        for (DuelRequest request : requests) {
            if (request.getRequestee().equals(one) && request.getRequester().equals(two)) return request;
            if (request.getRequestee().equals(two) && request.getRequester().equals(one)) return request;
        }
        return null;
    }
}
