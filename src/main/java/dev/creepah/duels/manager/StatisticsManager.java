package dev.creepah.duels.manager;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import com.zaxxer.hikari.pool.HikariPool;
import dev.creepah.duels.model.DuelKit;
import dev.creepah.duels.stats.SQLConfig;
import dev.creepah.duels.stats.Statistic;

import java.sql.*;
import java.util.UUID;

public class StatisticsManager {

    private HikariPool pool;
    public int K_FACTOR = 50;

    // Establishes a connection with the database.
    public StatisticsManager(SQLConfig config) {
        HikariConfig h = new HikariConfig();
        h.setJdbcUrl("jdbc:mysql://" + config.getHostname() + ":" + config.getPort() + "/" + config.getDatabase());
        h.setDriverClassName("com.mysql.jdbc.Driver");
        h.setUsername(config.getUsername());
        h.setPassword(config.getPassword());
        pool = new HikariPool(new HikariDataSource(h));
    }

    // Issues a statement to create a table for the specified kit.
    public void createTable(DuelKit kit) throws SQLException {
        try (Connection c = pool.getConnection()) {
            if (!c.getMetaData().getTables(null, null, kit.tableName(), null).next()) {
                try (Statement s = c.createStatement()) {
                    s.executeUpdate("CREATE TABLE " + kit.tableName() + " (" +
                            "id INT(32) AUTO_INCREMENT, " +
                            "uuid VARCHAR(255) NOT NULL, " +
                            "wins INT(32) NOT NULL DEFAULT 0, " +
                            "losses INT(32) NOT NULL DEFAULT 0, " +
                            "rating INT(32) NOT NULL DEFAULT 1500, " +
                            "PRIMARY KEY (id))");

                    s.close();
                } finally {
                    c.close();
                }
            }
        }
    }

    // Issues a statement to grab a statistic value from the database.
    public int getStat(UUID uuid, DuelKit kit, Statistic statistic) throws SQLException {
        try (Connection c = pool.getConnection()) {
            try (PreparedStatement s = c.prepareStatement("SELECT " + str(statistic) + " FROM " + kit.tableName() + " WHERE uuid = ?")) {
                s.setString(1, uuid.toString());

                try (ResultSet set = s.executeQuery()) {
                    if (!set.next()) {
                        try (PreparedStatement st = c.prepareStatement("INSERT INTO " + kit.tableName() + " (uuid, wins, losses, rating) VALUES (?, ?, ?, ?)")) {
                            st.setString(1, uuid.toString());
                            st.setInt(2, Statistic.WINS.getDefaultValue());
                            st.setInt(3, Statistic.LOSSES.getDefaultValue());
                            st.setInt(4, Statistic.RATING.getDefaultValue());

                            st.executeUpdate();
                            st.close();

                            return statistic.getDefaultValue();
                        }
                    } else return set.getInt(1);
                }
            } finally {
                c.close();
            }
        }
    }

    // Issues a statement to set the value of a statistic in the database.
    public void setStat(UUID uuid, DuelKit kit, Statistic statistic, int value) throws SQLException {
        try (Connection c = pool.getConnection()) {
            try (PreparedStatement s = c.prepareStatement("UPDATE " + kit.tableName() + " SET " + str(statistic) + " = ? WHERE uuid = ?")) {
                s.setInt(1, value);
                s.setString(2, uuid.toString());

                s.executeUpdate();
                s.close();
            } finally {
                c.close();
            }
        }
    }

    // Clears a player's data from the specified kit's table.
    public void clear(UUID uuid, DuelKit kit) throws SQLException {
        try (Connection c = pool.getConnection()) {
            try (PreparedStatement s = c.prepareStatement("DELETE FROM " + kit.tableName() + " WHERE uuid = ?")) {
                s.setString(1, uuid.toString());

                s.executeUpdate();
                s.close();
            } finally {
                c.close();
            }
        }
    }

    // Drops the table which stores statistics for the specified kit.
    public void drop(DuelKit kit) throws SQLException {
        try (Connection c = pool.getConnection()) {
            try (PreparedStatement s = c.prepareStatement("DROP TABLE " + kit.tableName())) {
                s.executeUpdate();
                s.close();
            } finally {
                c.close();
            }
        }
    }

    // Returns updated ELO ratings for the provided winner and loser.
    public int[] getNewValues(double winnersRating, double losersRating) {
        // These are the transformed ratings. This is to simplify upcoming calculations.
        double wT = Math.pow(10, winnersRating / 400);
        double lT = Math.pow(10, losersRating / 400);

        // These are the expected scores for each player.
        double wE = wT / (wT + lT);
        double lE = lT / (wT + lT);

        // These are the new calculated ratings.
        double winnerNew = winnersRating + K_FACTOR * (1 - wE);
        double loserNew = losersRating + K_FACTOR * (0 - lE);

        return new int[] { (int) winnerNew, (int) loserNew };
    }

    public String str(Statistic statistic) {
        return statistic.getString().toLowerCase();
    }
}
