package dev.creepah.duels.manager;

import com.google.gson.Gson;
import dev.creepah.duels.RebootDuels;
import dev.creepah.duels.model.Arena;
import dev.creepah.duels.model.Duel;
import lombok.Getter;
import tech.rayline.core.GsonBridge;
import tech.rayline.core.inject.Inject;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class ArenaManager {

    @Inject private Gson gson = new GsonBridge(RebootDuels.get()).getGson();
    @Getter private Set<Arena> arenas = new HashSet<>();

    private File directory = new File(RebootDuels.get().getDataFolder(), "arenas");

    // Load arenas from file.
    public void loadArenas() throws IOException {
        if (!directory.exists() && !directory.isDirectory() && !directory.mkdirs())
            throw new IOException("Failed to create arenas directory!");

        for (String f : directory.list(((dir, name) -> name.endsWith(".json")))) {
            try {
                arenas.add(RebootDuels.get().load(Arena.class, new File(directory, f)));
                RebootDuels.get().getLogger().info("Loaded arena data from " + f);
            } catch (Exception e) {
                e.printStackTrace();
                RebootDuels.get().getLogger().info("Failed to load arena from " + f + "!");
            }
        }
    }

    // Save arenas to file.
    public void saveArenas() throws IOException {
        for (Arena arena : getArenas()) {
            String data = gson.toJson(arena);
            FileWriter writer = new FileWriter(new File(directory, arena.getName().toLowerCase() + ".json"));

            writer.write(data);
            writer.flush();
            writer.close();
        }

        RebootDuels.get().getLogger().info("Saved " + getArenas().size() + " arenas to file!");
    }

    // Returns an arena not currently being used.
    public Arena getFreeArena() {
        for (Arena arena : getArenas()) {
            if (!RebootDuels.get().getDuelsManager().getActiveDuels().keySet().contains(arena)) return arena;
        }
        return null;
    }

    // Returns an arena with the specified name.
    public Arena getByName(String name) {
        for (Arena arena : getArenas()) if (arena.getName().equalsIgnoreCase(name)) return arena;
        return null;
    }

    // Returns the arena associated with the specified duel.
    public Arena getAssociatedArena(Duel duel) {
        Map<Arena, Duel> duels = RebootDuels.get().getDuelsManager().getActiveDuels();
        for (Arena arena : duels.keySet()) if (duels.get(arena).equals(duel)) return arena;
        return null;
    }
}
