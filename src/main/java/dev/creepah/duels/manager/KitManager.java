package dev.creepah.duels.manager;

import com.google.gson.Gson;
import dev.creepah.duels.RebootDuels;
import dev.creepah.duels.model.DuelKit;
import lombok.Getter;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import tech.rayline.core.GsonBridge;
import tech.rayline.core.inject.Inject;
import tech.rayline.core.util.PlayerUtil;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.Set;

public class KitManager {

    @Inject private Gson gson = new GsonBridge(RebootDuels.get()).getGson();
    @Getter private Set<DuelKit> kits = new HashSet<>();

    private File directory = new File(RebootDuels.get().getDataFolder(), "kits");

    // Load kits from file.
    public void loadKits() throws IOException {
        if (!directory.exists() && !directory.isDirectory() && !directory.mkdirs())
            throw new IOException("Failed to create arenas directory!");

        for (String f : directory.list(((dir, name) -> name.endsWith(".json")))) {
            try {
                kits.add(RebootDuels.get().load(DuelKit.class, new File(directory, f)));
                RebootDuels.get().getLogger().info("Successfully loaded kit from " + f);
            } catch (Exception e) {
                e.printStackTrace();
                RebootDuels.get().getLogger().info("Failed to load kit from " + f + "!");
            }
        }
    }

    // Save kits to file.
    public void saveKits() throws IOException {
        for (DuelKit kit : kits) {
            String data = gson.toJson(kit);
            FileWriter writer = new FileWriter(new File(directory, kit.getName().toLowerCase() + ".json"));

            writer.write(data);
            writer.flush();
            writer.close();
        }
    }

    // Creates a new duel kit.
    // The code is a future-proof feature where certain inputted numbers will mean certain things which modify the kit.
    // 1 : The kit doesn't give the player any items and allows the player to fight using their own.
    public DuelKit create(String name, int code) {
        DuelKit kit = new DuelKit(name);
        kit.setCustom(code == 1);
        getKits().add(kit);

        try {
            RebootDuels.get().getStatsManager().createTable(kit);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return kit;
    }

    // Give the specified player the specified kit.
    public void give(Player player, DuelKit kit) {
        PlayerInventory inventory = player.getInventory();
        // If the kit is custom, save the player's inventory and give it straight back to them.
        if (kit.isCustom()) {
            ItemStack[] contents = inventory.getContents();
            ItemStack[] armour = inventory.getArmorContents();

            PlayerUtil.resetPlayer(player);

            inventory.setContents(contents);
            inventory.setArmorContents(armour);
        } else { // Otherwise give the player the contents of the kit.
            PlayerUtil.resetPlayer(player);

            for (DuelKit.KitItem item : kit.getContents()) {
                if (item.getStack() == null) continue;
                inventory.setItem(item.getSlot(), item.getStack());
            }
            inventory.setArmorContents(kit.getArmour());
        }
    }

    // Removes the specified kit from memory and drops its SQL database.
    public void remove(DuelKit kit) {
        getKits().remove(kit);

        try {
            RebootDuels.get().getStatsManager().drop(kit);
            File file = new File(new File(RebootDuels.get().getDataFolder(), "kits"), kit.getName().toLowerCase() + ".json");
            Files.delete(file.toPath());
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    // Returns a kit from specified name.
    public DuelKit getKit(String name) {
        for (DuelKit kit : getKits()) if (kit.getName().equalsIgnoreCase(name)) return kit;
        return null;
    }
}
