package dev.creepah.duels.gui;

import dev.creepah.duels.RebootDuels;
import dev.creepah.duels.manager.QueueManager;
import dev.creepah.duels.model.DuelKit;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import tech.rayline.core.command.EmptyHandlerException;
import tech.rayline.core.gui.ClickAction;
import tech.rayline.core.gui.InventoryGUI;
import tech.rayline.core.gui.InventoryGUIButton;
import tech.rayline.core.util.ItemShorthand;
import tech.rayline.core.util.SoundUtil;

public class KitSelectionGUI extends InventoryGUI {

    /*
    Represents the kit selection GUI shown to the player when they enter a queue or request to duel someone.
     */

    private Player target;

    public KitSelectionGUI(Player target) {
        super(RebootDuels.get(),
                (int) (Math.ceil((double) RebootDuels.get().getKitManager().getKits().size() / 9D) * 9),
                ChatColor.RED + "Select a Duel Kit");

        this.target = target;

        RebootDuels.get().getKitManager().getKits().forEach(kit -> addButton(new KitButton(kit)));

        updateInventory();
    }

    // Represents a kit button which shows the name, icon and no. of players in the queue.
    public class KitButton extends InventoryGUIButton {

        private DuelKit kit;

        public KitButton(DuelKit kit) {
            this.kit = kit;

            QueueManager queueManager = RebootDuels.get().getQueueManager();
            int queue = queueManager.getQueue().get(kit) == null ? 0 : queueManager.getQueue().get(kit).size();

            ItemShorthand stack = ItemShorthand.setMaterial(kit.getIcon()).setName("&c" + kit.getName());
            if (target == null) stack.withLore("&7" + queue + " in queue");

            setStack(stack.get());
        }

        @Override
        public void onPlayerClick(Player player, ClickAction action) throws EmptyHandlerException {
            // If no target is specified, add to the queue. Otherwise request the target.
            if (target == null) {
                RebootDuels.get().getQueueManager().addTo(kit, player);
            } else RebootDuels.get().getRequestManager().request(player, target, kit);
            SoundUtil.playTo(player, Sound.NOTE_PIANO);
            player.closeInventory();
        }
    }
}
