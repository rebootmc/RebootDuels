package dev.creepah.duels.cmd.sub;

import dev.creepah.duels.RebootDuels;
import dev.creepah.duels.manager.KitManager;
import dev.creepah.duels.model.DuelKit;
import dev.creepah.duels.model.RebootDuelsCommand;
import org.bukkit.entity.Player;
import tech.rayline.core.command.ArgumentRequirementException;
import tech.rayline.core.command.CommandException;
import tech.rayline.core.command.CommandPermission;

@CommandPermission("reboot.duels.addkit")
public class AddKitSub extends RebootDuelsCommand {

    public AddKitSub() {
        super("addkit");
    }

    @Override
    protected void handleCommand(Player player, String[] args) throws CommandException {
        if (args.length == 0) throw new ArgumentRequirementException("Invalid arguments! Usage: /duel addkit <name> [code]");

        KitManager manager = RebootDuels.get().getKitManager();
        DuelKit kit = manager.getKit(args[0]);
        String action = "updated";

        if (kit == null) {
            int code = 0;

            if (args.length > 1) {
                try {
                    code = Integer.parseInt(args[1]);
                    player.sendMessage(RebootDuels.get().formatAt("kit.code-used").withModifier("code", code).get());
                } catch (NumberFormatException ex) {
                    throw new CommandException("Invalid second argument! Please specify a number!");
                }
            }

            kit = manager.create(args[0], code);
            action = "created";
        }

        kit.loadFrom(player, !kit.isCustom());
        player.sendMessage(RebootDuels.get().formatAt("kit.set").withModifier("action", action).withModifier("name", kit.getName()).get());
    }
}
