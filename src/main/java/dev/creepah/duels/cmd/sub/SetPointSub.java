package dev.creepah.duels.cmd.sub;

import dev.creepah.duels.RebootDuels;
import dev.creepah.duels.model.Arena;
import dev.creepah.duels.model.RebootDuelsCommand;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import tech.rayline.core.command.ArgumentRequirementException;
import tech.rayline.core.command.CommandException;
import tech.rayline.core.command.CommandPermission;

@CommandPermission("reboot.duels.setpoint")
public class SetPointSub extends RebootDuelsCommand {

    public SetPointSub() {
        super("setpoint");
    }

    @Override
    protected void handleCommand(Player player, String[] args) throws CommandException {
        if (args.length < 2) throw new ArgumentRequirementException("Invalid arguments! Usage: /duel setpoint <name> <1:2>");

        Arena arena = RebootDuels.get().getArenaManager().getByName(args[0]);
        if (arena == null) {
            arena = new Arena(args[0]);
            RebootDuels.get().getArenaManager().getArenas().add(arena);
            player.sendMessage(RebootDuels.get().formatAt("arena.new").withModifier("name", args[0]).get());
        }

        int position;
        try {
            position = Integer.parseInt(args[1]);
        } catch (NumberFormatException e) {
            throw new CommandException("Invalid position specified, please enter a number!");
        }

        if (position == 0 || position > 2) throw new CommandException("Invalid position specified, please enter 1 or 2!");

        Location loc = player.getLocation();
        if (position == 1) arena.setLocationOne(loc);
        else arena.setLocationTwo(loc);

        player.sendMessage(RebootDuels.get().formatAt("arena.set-pos")
                .withModifier("position", position)
                .withModifier("arena", arena.getName())
                .get());
    }
}
