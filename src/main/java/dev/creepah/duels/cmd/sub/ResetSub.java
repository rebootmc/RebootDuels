package dev.creepah.duels.cmd.sub;

import dev.creepah.duels.RebootDuels;
import dev.creepah.duels.model.DuelKit;
import dev.creepah.duels.model.RebootDuelsCommand;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import tech.rayline.core.command.ArgumentRequirementException;
import tech.rayline.core.command.CommandException;
import tech.rayline.core.command.CommandPermission;

import java.sql.SQLException;

@CommandPermission("reboot.duels.reset")
public class ResetSub extends RebootDuelsCommand {

    public ResetSub() {
        super("reset");
    }

    @Override
    protected void handleCommand(Player player, String[] args) throws CommandException {
        if (args.length == 0) throw new ArgumentRequirementException("Invalid arguments! Usage: /duel reset <kit> [player]");

        DuelKit kit = RebootDuels.get().getKitManager().getKit(args[0]);
        if (kit == null) throw new CommandException("Invalid kit specified!");

        OfflinePlayer target;
        if (args.length == 1) target = player;
        else target = Bukkit.getOfflinePlayer(args[1]);

        try {
            RebootDuels.get().getStatsManager().clear(target.getUniqueId(), kit);
            player.sendMessage(RebootDuels.get().formatAt("duel.reset")
                    .withModifier("name", target.getName())
                    .withModifier("kit", kit.getName()).get());
        } catch (SQLException e) {
            e.printStackTrace();
            throw new CommandException("Failed to retrieve statistics from the database!");
        }
    }
}
