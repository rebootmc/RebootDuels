package dev.creepah.duels.cmd.sub;

import dev.creepah.duels.RebootDuels;
import dev.creepah.duels.model.DuelRequest;
import dev.creepah.duels.model.RebootDuelsCommand;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import tech.rayline.core.command.CommandException;
import tech.rayline.core.command.CommandPermission;

@CommandPermission("reboot.duels.deny")
public class DenySub extends RebootDuelsCommand {

    public DenySub() {
        super("deny");
    }

    @Override
    protected void handleCommand(Player player, String[] args) throws CommandException {
        if (args.length == 0) throw new CommandException("Invalid arguments! Usage: /duel deny <name>");

        Player target = Bukkit.getPlayer(args[0]);
        if (target == null) throw new CommandException("The player " + args[0] + " could not be found!");

        DuelRequest duelRequest = RebootDuels.get().getRequestManager().getRequestInvolving(player, target);

        if (duelRequest == null)
            throw new CommandException("That player has not sent you a request to duel!");

        duelRequest.deny();
    }
}
