package dev.creepah.duels.cmd.sub;

import dev.creepah.duels.RebootDuels;
import dev.creepah.duels.manager.QueueManager;
import dev.creepah.duels.model.RebootDuelsCommand;
import org.bukkit.entity.Player;
import tech.rayline.core.command.CommandException;
import tech.rayline.core.command.CommandPermission;

@CommandPermission("reboot.duels.leave")
public class LeaveSub extends RebootDuelsCommand {

    public LeaveSub() {
        super("leave");
    }

    @Override
    protected void handleCommand(Player player, String[] args) throws CommandException {
        QueueManager queueManager = RebootDuels.get().getQueueManager();

        if (queueManager.getQueue(player) != null) {
            queueManager.removeFrom(player);
            player.sendMessage(RebootDuels.get().formatAt("duel.removed").get());
        } else {
            player.sendMessage(RebootDuels.get().formatAt("duel.not-in-queue").get());
        }
    }
}
