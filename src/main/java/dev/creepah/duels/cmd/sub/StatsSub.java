package dev.creepah.duels.cmd.sub;

import dev.creepah.duels.RebootDuels;
import dev.creepah.duels.manager.StatisticsManager;
import dev.creepah.duels.model.DuelKit;
import dev.creepah.duels.model.RebootDuelsCommand;
import dev.creepah.duels.stats.Statistic;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import tech.rayline.core.command.CommandException;
import tech.rayline.core.command.CommandPermission;
import tech.rayline.core.plugin.Formatter;

import java.sql.SQLException;
import java.util.UUID;

@CommandPermission("reboot.duels.stats")
public class StatsSub extends RebootDuelsCommand {

    public StatsSub() {
        super("stats");
    }

    @Override
    protected void handleCommand(Player player, String[] args) throws CommandException {
        OfflinePlayer target;
        if (args.length == 0) target = player;
        else target = Bukkit.getOfflinePlayer(args[0]);

        StatisticsManager statsManager = RebootDuels.get().getStatsManager();
        UUID uuid = target.getUniqueId();

        player.sendMessage(RebootDuels.get().formatAt("duel.stat-title")
                .withModifier("name", target.getName())
                .get());

        for (DuelKit kit : RebootDuels.get().getKitManager().getKits()) {
            try {
                Formatter.FormatBuilder builder = RebootDuels.get().formatAt("duel.stat");
                builder.withModifier("kit", kit.getName());
                for (Statistic stat : Statistic.values()) {
                    builder.withModifier(stat.getString().toLowerCase(), statsManager.getStat(uuid, kit, stat));
                }

                player.sendMessage(builder.get());

            } catch (SQLException e) {
                e.printStackTrace();
                throw new CommandException("Failed to retrieve statistics from the database!");
            }
        }
    }
}
