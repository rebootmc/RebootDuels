package dev.creepah.duels.cmd.sub;

import dev.creepah.duels.RebootDuels;
import dev.creepah.duels.manager.KitManager;
import dev.creepah.duels.model.DuelKit;
import dev.creepah.duels.model.RebootDuelsCommand;
import org.bukkit.entity.Player;
import tech.rayline.core.command.ArgumentRequirementException;
import tech.rayline.core.command.CommandException;
import tech.rayline.core.command.CommandPermission;

@CommandPermission("reboot.duels.delkit")
public class DelKitSub extends RebootDuelsCommand {

    public DelKitSub() {
        super("delkit");
    }

    @Override
    protected void handleCommand(Player player, String[] args) throws CommandException {
        if (args.length == 0) throw new ArgumentRequirementException("Invalid arguments! Usage: /duel delkit <name>");

        KitManager manager = RebootDuels.get().getKitManager();
        DuelKit kit = manager.getKit(args[0]);

        if (kit == null) throw new CommandException("Invalid kit specified!");

        manager.remove(kit);

        player.sendMessage(RebootDuels.get().formatAt("kit.deleted")
                .withModifier("name", kit.getName())
                .withModifier("kit", kit.getName())
                .get());
    }
}
