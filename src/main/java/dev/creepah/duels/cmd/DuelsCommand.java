package dev.creepah.duels.cmd;

import dev.creepah.duels.RebootDuels;
import dev.creepah.duels.cmd.sub.*;
import dev.creepah.duels.gui.KitSelectionGUI;
import dev.creepah.duels.manager.DuelsManager;
import dev.creepah.duels.manager.RequestManager;
import dev.creepah.duels.model.RebootDuelsCommand;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import tech.rayline.core.command.CommandException;
import tech.rayline.core.command.CommandMeta;
import tech.rayline.core.command.CommandPermission;

import java.util.List;
import java.util.stream.Collectors;

@CommandPermission("reboot.duels.command")
@CommandMeta(description = "Root command for Duels", aliases = "rd")
public class DuelsCommand extends RebootDuelsCommand {

    public DuelsCommand() {
        super("duel",
                new SetPointSub(),
                new AddKitSub(),
                new StatsSub(),
                new DelKitSub(),
                new ResetSub(),
                new LeaveSub(),
                new AcceptSub(),
                new DenySub());
    }

    @Override
    protected void handleCommand(Player player, String[] args) throws CommandException {
        DuelsManager duelsManager = RebootDuels.get().getDuelsManager();
        RequestManager requestManager = RebootDuels.get().getRequestManager();

        if (args.length == 0) {
            if (RebootDuels.get().getQueueManager().getQueue(player) == null) {
                if (duelsManager.getDuelPlayer(player) != null)
                    throw new CommandException("You cannot join the queue while already in a duel!");

                new KitSelectionGUI(null).openFor(player);
            } else player.sendMessage(RebootDuels.get().formatAt("duel.in-queue").get());
            return;
        }

        Player target = Bukkit.getPlayer(args[0]);
        if (target == null)
            throw new CommandException("The player " + args[0] + " could not be found!");

        // Check that a request involving these two doesn't already exist.
        if (requestManager.getRequestInvolving(player, target) != null)
            throw new CommandException("There is already a request involving you and " + target.getName() + "!");

        // Check that they don't try to duel themselves.
        if (player == target) throw new CommandException("You cannot duel yourself!");

        // Check that the player or target isn't already in a duel.
        if (duelsManager.getDuel(player) != null)
            throw new CommandException("You're already in a duel!");

        if (duelsManager.getDuel(target) != null)
            throw new CommandException(target.getName() + " is already in a duel!");

        new KitSelectionGUI(target).openFor(player);
    }

    @Override
    protected List<String> handleTabComplete(CommandSender sender, Command command, String alias, String[] args) {
        String n = args[0].toLowerCase().trim();

        return Bukkit.getOnlinePlayers().stream()
                .filter(p -> p.getName().toLowerCase().startsWith(n))
                .map(Player::getName)
                .collect(Collectors.toList());
    }
}
